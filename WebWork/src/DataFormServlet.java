import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@WebServlet(name = "DataFormServlet", urlPatterns = {"/DataFormServlet"})
public class DataFormServlet  extends HttpServlet {
    
    HttpSession session;
    private final String PARAM_MAP = "mainMap";
    private final String PARAM_NAME = "name";
    private final String PARAM_VALUE = "value";
    private static final String ACTION = "action";
    private static final String DELETE = "delete";
    Map<String, HashSet<String>> mainMap;

    @Override
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        session = req.getSession();
        String name = req.getParameter(PARAM_NAME);
        String value = req.getParameter(PARAM_VALUE);
        if (session.getAttribute(PARAM_MAP) == null) {
            Map<String, HashSet<String>> mainMap = new HashMap<>();
            session.setAttribute(PARAM_MAP, mainMap);
        }

        mainMap = (HashMap<String, HashSet<String>>) session.getAttribute(PARAM_MAP);
        HashSet<String> list;
        if (!mainMap.containsKey(name)) {
            list = new HashSet<>();
            list.add(value);
            mainMap.put(name, list);
        } else {
            list = mainMap.get(name);
            list.add(value);
        }

        getServletContext().getRequestDispatcher("/dataForm.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        if (parameterMap.containsKey(ACTION) && parameterMap.get(ACTION)[0].equals(DELETE)){            
            doDelete(req, resp);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> parameterMap = req.getParameterMap();
        mainMap.get(parameterMap.get("k")[0]).remove(parameterMap.get("v")[0]);
        getServletContext().getRequestDispatcher("/dataForm.jsp").forward(req, resp);
    }
}
