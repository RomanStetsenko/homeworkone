import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@WebServlet(name = "WebFileManagerServlet", urlPatterns = {"/WebFileManager"})
public class WebFileManagerServlet extends HttpServlet {
    private List<String> textTypes = Arrays.asList("txt");
    private List<String> historyMoves = new ArrayList<>(Arrays.asList(""));
    private int currentIndexMove = 0;
    private static final String PARAM_PATH = "path";
    private static final String PARAM_DIRECTION = "direction";
    private static final String PARAM_DIRECTION_VALUE = "back";
    private static final String PARAM_NEW_FILE = "new_file";
    private static final String PARAM_NEW_FILE_VALUE = "new";
    private static final String PARAM_DELETE_FILE = "delete_file";
    private String os;
    private String browserDetails;
    private StringBuilder send;
    private String mainPath = "";
    private PrintWriter out;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        out = resp.getWriter();
        browserDetails = req.getHeader("User-Agent");
        checkOS();
        out.println("Your Operating System: <b>" + os + "</b> ");
        out.println("</br><a href=\"index.jsp\">[return to Index page]</a></br></br>");
        send = new StringBuilder();
        if (os.equalsIgnoreCase("windows")) {
            Map<String, String[]> parameterMap = req.getParameterMap();
            if (parameterMap.size()>0){
                if (parameterMap.containsKey(PARAM_DIRECTION) && parameterMap.get(PARAM_DIRECTION)[0].equals(PARAM_DIRECTION_VALUE)){
                    historyMoves.remove(currentIndexMove);
                    --currentIndexMove;
                    mainPath = getMove();
                } else if (parameterMap.containsKey(PARAM_NEW_FILE) && parameterMap.get(PARAM_NEW_FILE)[0].equals(PARAM_NEW_FILE_VALUE)) {
                    mainPath = getMove();
                    createNewFile();
                } else if (parameterMap.containsKey(PARAM_DELETE_FILE)) {
                    mainPath = getMove();
                    deleteFile(parameterMap.get(PARAM_DELETE_FILE)[0]);
                } else {
                    historyMoves.add(parameterMap.get(PARAM_PATH)[0]);
                    mainPath += parameterMap.get(PARAM_PATH)[0];
                    ++currentIndexMove;
                }
                printListByPath(mainPath);
            } else {
                mainPath = "";
                currentIndexMove = 0;
                historyMoves.clear();
                historyMoves.add("");
                printListRoots();
            }
        } else {
            send.append("Only Windows support");
        }
        out.println("</br>" + send.toString());
        send.setLength(0);
    }

    private String getMove(){
        StringBuilder result = new StringBuilder();
        for (String move : historyMoves) {
            result.append(move);
        }
//        historyMoves.forEach(result::append); //java 8
        return  result.toString();
    }

    private void printListByPath(String path){
        //if "path" is empty print root folder
        if (path.equals("")){
            printListRoots();
            return;
        }

        File fileOrDirPath = new File(path);
        StringBuilder result = new StringBuilder("</br>");
        if (fileOrDirPath.isFile()){
            String extension = fileOrDirPath.getName().substring(fileOrDirPath.getName().lastIndexOf(".") + 1, fileOrDirPath.getName().length());
            result
                    .append("Path to file: ").append(fileOrDirPath.getPath())
                    .append("</br>\n")
                    .append("File name: ").append(fileOrDirPath.getName());
            if (textTypes.contains(extension)){
                result
                        .append("</br><p>")
                        .append(readFile(fileOrDirPath))
                        .append("</p>");
            }
        } else if (fileOrDirPath.isDirectory() | fileOrDirPath.isAbsolute()){
            //print directories and files by "path"
            File[] listOfFiles = fileOrDirPath.listFiles();
            if (listOfFiles != null && listOfFiles.length>0){
                for (File file : listOfFiles) {
                    if (file.isFile()) {
                        result.append(String.format("<a href=\"?%s=%s\">%s</a> &nbsp;<a href=?%s=%s>[x]</a></br>\n", PARAM_PATH, file.getName(), file.getName(), PARAM_DELETE_FILE, file.getName()));
                    } else if (file.isDirectory()) {
                        result.append(String.format("<a href=\"?%s=%s\\\">%s\\</a></br>\n", PARAM_PATH, file.getName(),file.getName()));
                    }
                }
            } else {
                result.append("Dir is empty.");
            }
        }
        //print back link
        out.println(String.format("<a href=\"?%s=%s\">[go_back]</a></br>\n", PARAM_DIRECTION, PARAM_DIRECTION_VALUE));

        //print create new file link
        if (fileOrDirPath.isDirectory()){
            out.println(String.format("<a href=\"?%s=%s\">[new_file]</a></br>\n", PARAM_NEW_FILE, PARAM_NEW_FILE_VALUE));
        }
        out.println(result);
    }

    private void printListRoots(){
        File[] drives = File.listRoots();
        if (drives != null && drives.length > 0) {
            for (File drive : drives) {
                send
                        .append("<a href=\"?")
                        .append(PARAM_PATH + "=")
                        .append(drive)
                        .append("\">")
                        .append(drive)
                        .append("</a>")
                        .append("</br>")
                        .append("\n");
            }
        } else {
            send.append("Nothing to show");
        }
    }

    private String readFile(File file){
        StringBuilder textOfFile = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                textOfFile.append(line);
                textOfFile.append(System.lineSeparator());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textOfFile.toString();
    }

    private void createNewFile() throws IOException {
        String newFileName = "new_file_" + System.currentTimeMillis();
        File newFile = new File(mainPath, newFileName);
        boolean resultOfFileCreating = newFile.createNewFile();
        if (!resultOfFileCreating){
            out.println("</br><p>Error: File: \""+ newFileName+"\" already exist!</p>");
        }
    }

    private void deleteFile(String fileName){
        File fileToDelete = new File(mainPath, fileName);
        if (!fileToDelete.delete()){
            out.println("</br><p>Error: File: \""+ fileName+"\" was not deleted!</p>");
        }
    }

    private void checkOS(){
        if (browserDetails.toLowerCase().contains("windows")) {
            os = "Windows";
        } else if (browserDetails.toLowerCase().contains("mac")) {
            os = "Mac";
        } else if (browserDetails.toLowerCase().contains("x11")) {
            os = "Unix";
        } else if (browserDetails.toLowerCase().contains("android")) {
            os = "Android";
        } else if (browserDetails.toLowerCase().contains("iphone")) {
            os = "IPhone";
        } else {
            os = "UnKnown, More-Info: " + browserDetails;
        }
    }
}
