<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title></title>
</head>
<body>
<a href="index.jsp">[return to Index page]</a></br></br>
<table border="1">
    <th>Name</th>     <th>Value</th>     <th>Action</th>
    <c:forEach var="entry" items="${mainMap}">
        <c:set var="i" value="0"/>
        <c:set var="str" value="empty"/>
        <c:forEach var="valuesSet" items="${entry.value}">

            <c:choose>
                <c:when test="${i == 0}">
                    <c:set var="str" value="${entry.key}"/>
                    <c:set var="i" value="1"/>
                </c:when>
                <c:otherwise>
                    <c:set var="str" value=""/>
                    <c:set var="i" value="1"/>
                </c:otherwise>
            </c:choose>
            
            <tr><td>${str}</td> <td>${valuesSet}</td> <td><a href="?action=delete&k=${entry.key}&v=${valuesSet}">delete</a></td></tr>
        </c:forEach>
    </c:forEach>
    <tr>
        <form action="/DataFormServlet" method="post">
            <td>
                <input name="name" type="text">
            </td>
            <td>
                <input name="value" type="text">
            </td>
            <td>
                <input type="submit" value="add">
            </td>
        </form>
    </tr>    
</table>
</body>
</html>
