package geekhub.lesson4;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        //        4.1
        SetOperationsImpl setOperations = new SetOperationsImpl();
//        test with Strings
//        Set<String> a = new HashSet<>(Arrays.asList("one","two","three","four"));
//        Set<String> b = new HashSet<>(Arrays.asList("two","four","five","six","seven"));

        //test with class Student
        Set<Student> a = new HashSet<>(Arrays.asList(new Student("Anatoly", 17, 400), new Student("Valik", 20, 720), new Student("Den", 21, 710), new Student("Ken", 18, 610)));
        Set<Student> b = new HashSet<>(Arrays.asList(new Student("Alex", 17, 400), new Student("Valik", 20, 720), new Student("Den", 19, 710), new Student("Ken", 18, 610)));

        System.out.println("\ncheck union");
        setOperations.union(a, b).forEach(System.out::println);

        System.out.println("\ncheck subtract");
        setOperations.subtract(a, b).forEach(System.out::println);

        System.out.println("\ncheck intersect");
        setOperations.intersect(a, b).forEach(System.out::println);

        System.out.println("\ncheck symmetricSubtract");
        setOperations.symmetricSubtract(a, b).forEach(System.out::println);

        //4.2
        Work work = new Work();
        System.out.println("\n----------------------print test Map<Date, Task>--------------------\n");
        for (Map.Entry entry : work.getWorkMap().entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        System.out.println("\n----------------------test getCategories()--------------------\n");
        System.out.println(work.getCategories());
        System.out.println("\n----------------------test getTasksByCategories()---------------------\n");
        for (Map.Entry entry : work.getTasksByCategories().entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }
        System.out.println("\n---------------------test getTasksByCategory(String category)----------------------\n");
        System.out.println(work.getTasksByCategory("minor"));
        System.out.println(work.getTasksByCategory("trivial"));
        System.out.println(work.getTasksByCategory("major"));
        System.out.println("\n---------------------test getTasksForToday()----------------------\n");
        System.out.println(work.getTasksForToday());
    }
}
