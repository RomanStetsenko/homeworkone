package geekhub.lesson4;

import java.util.*;

public class Work implements TaskManager {
    // 4.2
    private Map<Date, Task> work = new HashMap<>();

    public Map<Date, Task> getWorkMap() {
        return work;
    }

    public Work() {
        //initialization of test Tasks
        Task[] tasks = new Task[]{
                new Task("major", "---1---"),
                new Task("major", "---2---"),
                new Task("major", "---3---"),
                new Task("minor", "---4---"),
                new Task("minor", "---5---"),
                new Task("trivial", "---6---"),
        };

        //initialization of test Dates, adding created Tasks to them and setting that pair
        Date[] dates = new Date[tasks.length];
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR);
        for (int i = 0; i < tasks.length; i++) {
            cal.set(Calendar.HOUR, hour);
            dates[i] = cal.getTime();
            addTask(dates[i], tasks[i]); //setting
            hour += 6;
        }
    }

    @Override
    public void addTask(Date date, Task task) {
        work.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        work.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Collection<String> resultCategories = new ArrayList<>();
        for (Map.Entry<Date, Task> entrySet : work.entrySet()) {
            resultCategories.add(entrySet.getValue().getCategory());
        }
        return resultCategories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tasksByCategories = new HashMap<>();
        for (Map.Entry<Date, Task> entrySet : new TreeMap<>(work).entrySet()) {
            List<Task> list = tasksByCategories.get(entrySet.getValue().getCategory());
            if (list == null) {
                tasksByCategories.put(entrySet.getValue().getCategory(), new ArrayList<>(Arrays.asList(entrySet.getValue())));
            } else {
                list.add(entrySet.getValue());
                tasksByCategories.put(entrySet.getValue().getCategory(), list);
            }
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> list = new ArrayList<>();
        for (Map.Entry<Date, Task> entrySet : new TreeMap<>(work).entrySet()) {
            if (entrySet.getValue().getCategory().equals(category)) {
                list.add(entrySet.getValue());
            }
        }
        return list;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> list = new ArrayList<>();
        for (Map.Entry<Date, Task> entrySet : new TreeMap<>(work).entrySet()) {
            if (getDayFromDate(entrySet.getKey()) == getCurrentDay()) {
                list.add(entrySet.getValue());
            }
        }
        return list;
    }

    //auxiliary methods for 4.2
    private int getDayFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    private int getCurrentDay() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.DAY_OF_MONTH);
    }

}
