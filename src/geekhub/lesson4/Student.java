package geekhub.lesson4;

public class Student implements Comparable {
    private String name;
    private int age;
    private double scholarship;

    public Student(String name, int age, double scholarship) {
        this.name = name;
        this.age = age;
        this.scholarship = scholarship;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + Double.valueOf(scholarship).hashCode();
        result = prime * result + age;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Student){
            Student student = (Student) obj;
            if (name.equals(student.name) && age == student.age && scholarship == student.scholarship)
                return true;
        }
        return false;
    }

    @Override
    public int compareTo(Object o) {
        Student student = (Student) o;
        if (!this.name.equals(student.name))
            return this.name.compareTo(student.name);
        if (this.age != student.age)
            return Integer.valueOf(this.age).compareTo(student.age);
        return Double.valueOf(this.scholarship).compareTo(student.scholarship) * -1; // * -1 for descending order

    }

    @Override
    public String toString() {
        return String.format("name = %6s, age = %2d, scholarship = %6.2f", name, age, scholarship);
    }
}
