package geekhub.lesson4;

import java.util.HashSet;
import java.util.Set;

public class SetOperationsImpl implements SetOperations {
    // 4.1
    @Override
    public boolean equals(Set<?> a, Set<?> b) {
        return a == b || a != null && b != null && a.size() == b.size() && (a.containsAll(b));
    }

    @Override
    public Set union(Set a, Set b) {
        Set unionSet = new HashSet<>(a);//(Set) ((HashSet) a).clone();
        unionSet.addAll(b);
        return unionSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set subtractSet = new HashSet<>(a);//((Set) ((HashSet) a).clone());
        subtractSet.removeAll(b);
        return subtractSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set intersectSet = new HashSet<>(a);
        intersectSet.retainAll(b);
        return intersectSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set symmetricSubtractSet1 = new HashSet<>(a);
        Set symmetricSubtractSet2 = new HashSet<>(b);
        return union(subtract(symmetricSubtractSet1, symmetricSubtractSet2), subtract(symmetricSubtractSet2, symmetricSubtractSet1));
    }
}
