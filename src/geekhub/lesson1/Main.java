package geekhub.lesson1;

import java.util.Scanner;

public class Main {
    private long result = 1;
    private String numbersArray[] = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    private Scanner sc = new Scanner(System.in);

    public long factorial(int n) {
        if (n > 1) result *= n;
        return (n <= 1) ? result : factorial(--n);
    }

    public long fibonacci(int n) {
        if (n < 1) return 0;
        if (n == 1 || n == 2) return 1;
        long a = 1, b = 1, fibonacci = 1;
        for (int i = 3; i <= n; i++) {
            fibonacci = a + b;
            a = b;
            b = fibonacci;
        }
        return fibonacci;
    }

    public void printMainMenu() {
        System.out.println("Input \"1\" to use factorial (n!)");
        System.out.println("Input \"2\" to use Fibonacci sequence (1..n)");
        System.out.println("Input \"3\" to use number naming");
        System.out.println("Input \"stop\" for finish");
        System.out.println("Input \"up\" to come back to the beginning");
        System.out.print("\nChoose an action: ");
    }

    public void useFactorial() {
        while (true) {
            System.out.print("\nInput integer: ");
            if (sc.hasNextInt()) {
                int n = sc.nextInt();
                sc.nextLine();
                System.out.println(n + "! = " + factorial(n));
                result = 1;
            } else {
                String line = sc.nextLine();
                if (line.equalsIgnoreCase("stop")) System.exit(0);
                if (line.equalsIgnoreCase("up")) break;
                System.out.println(line + " - this is non-integer value");
            }
        }
    }

    public void useFibonacci() {
        while (true) {
            System.out.print("\nInput integer: ");
            if (sc.hasNextInt()) {
                int n = sc.nextInt();
                sc.nextLine();
                StringBuilder sb = new StringBuilder();
                sb.append("fibonacci:");
                for (int k = 0; k < n; k++) {
                    sb.append(fibonacci(k)).append(" ");
                }
                System.out.println(sb.toString());
            } else {
                String line = sc.nextLine();
                if (line.equalsIgnoreCase("stop")) System.exit(0);
                if (line.equalsIgnoreCase("up")) break;
                System.out.println(line + " - this is non-integer value");
            }
        }
    }

    public void useNumberNaming() {
        while (true) {
            System.out.print("\nInput integer from 0 to 9: ");
            if (sc.hasNextInt()) {
                int n = sc.nextInt();
                sc.nextLine();
                if (n > -1 && n < 10) {
                    System.out.print(numbersArray[n]);
                } else System.out.println(" out of range (0-9)");

            } else {
                String line = sc.nextLine();
                if (line.equalsIgnoreCase("stop")) System.exit(0);
                if (line.equalsIgnoreCase("up")) break;
                System.out.println(line + " - this is non-integer value");
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Main main = new Main();

        while (true) {
            main.printMainMenu();
            if (sc.hasNextInt()) {
                int i = sc.nextInt();
                sc.nextLine();
                if (i == 1) {
                    main.useFactorial();
                } else if (i == 2) {
                    main.useFibonacci();
                } else if (i == 3) {
                    main.useNumberNaming();
                } else System.out.println("Wrong action");
            } else {
                String line = sc.nextLine();
                if (line.equalsIgnoreCase("stop")) break;
                System.out.println("Try again");
            }
        }

    }
}
