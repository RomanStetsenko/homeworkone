package geekhub.lesson3.exceptions;

public class EmptyEnergyProviderException extends Exception {
    String energyProviderName;

    public EmptyEnergyProviderException(String energyProviderName) {
        this.energyProviderName = energyProviderName;
    }

    @Override
    public String toString() {
        return String.format("%s is empty%n", energyProviderName);
    }
}
