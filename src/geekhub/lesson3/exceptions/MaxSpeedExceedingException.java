package geekhub.lesson3.exceptions;

public class MaxSpeedExceedingException extends Exception {
    int maxSpeed;

    public MaxSpeedExceedingException(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String toString() {
        return String.format("Vehicle max speed is: %d miles per hour", maxSpeed);
    }
}
