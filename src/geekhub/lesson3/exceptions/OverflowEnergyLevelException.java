package geekhub.lesson3.exceptions;

public class OverflowEnergyLevelException extends Exception {
    int maxVolume;
    String energyProviderName;

    public OverflowEnergyLevelException(int maxVolume, String energyProviderName) {
        this.maxVolume = maxVolume;
        this.energyProviderName = energyProviderName;
    }

    @Override
    public String toString() {
        return String.format("%s max volume is: %d%n", energyProviderName, maxVolume);
    }
}
