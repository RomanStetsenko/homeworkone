package geekhub.lesson3;

import geekhub.lesson2.interfaces.Driveable;
import geekhub.lesson2.internal.ElectricEngine;
import geekhub.lesson2.internal.SolarBattery;
import geekhub.lesson3.exceptions.EmptyEnergyProviderException;
import geekhub.lesson3.exceptions.OverflowEnergyLevelException;
import geekhub.lesson3.internal.DieselEngine;
import geekhub.lesson3.internal.GasTank;
import geekhub.lesson3.objects.Boat;
import geekhub.lesson3.objects.Car;
import geekhub.lesson3.objects.SolarPoweredCar;
import geekhub.lesson3.objects.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    Scanner sc = new Scanner(System.in);

    Student students[] = {
            new Student("John", 17, 722.22),
            new Student("Doe", 18, 202.22),
            new Student("Joye", 17, 762.22),
            new Student("Jim", 19, 702.22),
            new Student("Valery", 19, 762.22),
            new Student("John", 17, 702.22),
            new Student("Net", 19, 792.22),
            new Student("John", 18, 742.22)
    };

    GasTank gasTanks[] = {
            new GasTank(100),
            new GasTank(50),
            new GasTank(500),
            new GasTank(200),
    };


    Comparable[] sort(Comparable[] elements) {
        ArrayList<Comparable> objects = new ArrayList<>(Arrays.asList(elements));
        Collections.sort(objects);
        return objects.toArray(new Comparable[objects.size()]);
    }

    String toStringArray(Comparable array[]){
        StringBuilder sb = new StringBuilder();
        for (Comparable each : array) {
            sb.append(each.toString()).append("\n");
        }
        return sb.toString();
    }

    void stringSpeedTest(){
        long startTime = System.currentTimeMillis();
        String test = "";
        for (int i= 0; i<100000; i++){
            test += " new String " + i;
        }
        long endTime = System.currentTimeMillis();
        System.out.printf("String testing, length = %d, Duration = %d %n", test.length(), (endTime - startTime));
    }

    void stringBuilderSpeedTest(){
        long startTime = System.currentTimeMillis();
        StringBuilder test = new StringBuilder();
        for (int i= 0; i<100000; i++){
            test.append(" new String ").append(i);
        }
        long endTime = System.currentTimeMillis();
        System.out.printf("StringBuilder testing, length = %d, Duration = %d %n", test.toString().length(), (endTime - startTime));
    }

    void stringBufferSpeedTest(){
        long startTime = System.currentTimeMillis();
        StringBuffer test = new StringBuffer();
        for (int i= 0; i<100000; i++){
            test.append(" new String ").append(i);
        }
        long endTime = System.currentTimeMillis();
        System.out.printf("StringBuffer testing, length = %d, Duration = %d %n", test.toString().length(), (endTime - startTime));
    }

    private static void printMainMenu() {
        System.out.println("Input \"1\" to do task 1");
        System.out.println("Input \"2\" to do task 2, Warning! test takes a few minutes");
        System.out.println("Input \"3\" to do task 3");
        System.out.println("Input \"q\" for finish\n");
    }

    private void printVehicleMenu() {
        System.out.println("Input \"1\" to choose car");
        System.out.println("Input \"2\" to choose solar-powered car");
        System.out.println("Input \"3\" to choose boat");
        System.out.println("Input \"q\" for finish\n");
    }

    private void printActionsMenu() {
        System.out.println("Input \"s\" for engine starting");
        System.out.println("Input \"g\" to start or increase moving");
        System.out.println("Input \"gg\" to powerful increase moving");
        System.out.println("Input \"t\" to turn the vehicle");
        System.out.println("Input \"b\" for braking");
        System.out.println("Input \"bb\" for hard braking");
        System.out.println("Input \"st\" to stop");
        System.out.println("Input \"a\" to add ten energy units");
        System.out.println("Input \"d\" to drive 10 miles");
        System.out.println("Input \"h\" to show travelled distance");
        System.out.println("Input \"e\" to show energy level");
        System.out.println("Input \"up\" to select another vehicle");
        System.out.println("Input \"q\" for finish");
        System.out.println("Input \"?\" to see actions menu\n");
    }

    void makeTaskOne(){
        System.out.println(toStringArray(students));
        Comparable sortedStudents[] = sort(students);
        System.out.println(toStringArray(students)); //to ensure that array is not changed
        System.out.println(toStringArray(sortedStudents));

        System.out.println(toStringArray(gasTanks));
        Comparable sortedTanks[] = sort(gasTanks);
        System.out.println(toStringArray(gasTanks)); //to ensure that array is not changed
        System.out.println(toStringArray(sortedTanks));
    }

    void makeTaskTwo(){
        stringSpeedTest();
        stringBuilderSpeedTest();
        stringBufferSpeedTest();
    }

    void makeTask3(){
        Car car = new Car(new GasTank(), new DieselEngine(400));
        Boat boat = new Boat(new GasTank(), new DieselEngine(150));
        SolarPoweredCar solarPoweredCar = new SolarPoweredCar(new SolarBattery(), new ElectricEngine());
        Driveable driveable = null;
        while (true){
            printVehicleMenu();
            String line = sc.nextLine();
            boolean isSelected;
            if (line.equalsIgnoreCase("1")) {
                driveable = car;
                isSelected = true;
            } else if (line.equalsIgnoreCase("2")) {
                driveable = solarPoweredCar;
                isSelected = true;
            } else if (line.equalsIgnoreCase("3")) {
                driveable = boat;
                isSelected = true;
            } else if (line.equalsIgnoreCase("q")) {
                break;
            } else {
                isSelected = false;
                System.out.println("Try again");
            }
            if (isSelected) {
                printActionsMenu();
                while (true) {
                    try{
                        String action = sc.nextLine();
                        if (action.equalsIgnoreCase("up")) break;
                        else if (action.equalsIgnoreCase("q")) System.exit(0);
                        else if (action.equalsIgnoreCase("s")) driveable.starting();
                        else if (action.equalsIgnoreCase("a")) driveable.addTenEnergyUnits();
                        else if (action.equalsIgnoreCase("d")) driveable.drive10miles();
                        else if (action.equalsIgnoreCase("h")) driveable.showTravelledDistance();
                        else if (action.equalsIgnoreCase("e")) driveable.showEnergyLevel();
                        else if (action.equalsIgnoreCase("?")) printActionsMenu();
                        else if (action.equalsIgnoreCase("g")) driveable.accelerate();
                        else if (action.equalsIgnoreCase("b")) driveable.brake();
                        else if (action.equalsIgnoreCase("gg")) driveable.powerfulAccelerate();
                        else if (action.equalsIgnoreCase("bb")) driveable.hardBraking();
                        else if (action.equalsIgnoreCase("t")) driveable.turn();
                        else if (action.equalsIgnoreCase("st")) driveable.stop();
                        else System.out.println("Try again");
                    } catch (OverflowEnergyLevelException e){
                        System.out.println("Can't add extra energy, because "+e.toString());
                    } catch (EmptyEnergyProviderException e) {
                        driveable.stop();
                        System.out.println(e.toString() + " You can use \"a\" command to add energy");
                    }
                }
            }
        }


    }

    public static void main(String[] args) {
        Main main = new Main();
        Scanner sc = new Scanner(System.in);

        while (true) {
            printMainMenu();
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("1")) main.makeTaskOne();
            else if (line.equalsIgnoreCase("2")) main.makeTaskTwo();
            else if (line.equalsIgnoreCase("3")) main.makeTask3();
            else if (line.equalsIgnoreCase("q")) break;
            else System.out.println("Try again");
        }

    }
}
