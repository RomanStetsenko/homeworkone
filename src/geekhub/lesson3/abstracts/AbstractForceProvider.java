package geekhub.lesson3.abstracts;

import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson3.exceptions.MaxSpeedExceedingException;

public abstract class AbstractForceProvider extends geekhub.lesson2.abstracts.AbstractForceProvider {

    public AbstractForceProvider(int maxSpeed) {
        super(maxSpeed);
    }

    @Override
    public void accelerating() {
        currentSpeed++;
        if (currentSpeed * 5 > maxSpeed) {
            try {
                throw new MaxSpeedExceedingException(maxSpeed);
            } catch (MaxSpeedExceedingException e) {
                System.out.println("Can't accelerate, because " + e.toString());
                currentSpeed--;
            }
        } else {
            energyProvider.increaseEnergyFeeding();
            for (ForceAcceptor each : forceAcceptor) {
                if (!each.getState())
                    each.roll();
            }
            System.out.format("Your speed is %d miles per hour%n", currentSpeed * 5);
        }
    }


}
