package geekhub.lesson3.abstracts;

import geekhub.lesson3.exceptions.EmptyEnergyProviderException;

public abstract class Vehicle extends geekhub.lesson2.abstracts.Vehicle {

    @Override
    public void drive10miles() throws EmptyEnergyProviderException {
        if (!forceProvider.isWorking()) {
            if (energyProvider.getVolume() > 0) {
                forceProvider.starting();
            } else {
                throw new EmptyEnergyProviderException(energyProvider.toString());
//                System.out.println(energyProvider.toString()+" is empty");
            }
        }
        while (forceProvider.getCurrentSpeed() < forceProvider.getMaxSpeed()) {
            forceProvider.accelerating();
        }
        forceProvider.setWayLimit(10);

    }
}
