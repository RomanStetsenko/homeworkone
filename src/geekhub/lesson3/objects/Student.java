package geekhub.lesson3.objects;

public class Student implements Comparable {
    private String name;
    private int age;
    private double scholarship;

    public Student(String name, int age, double scholarship) {
        this.name = name;
        this.age = age;
        this.scholarship = scholarship;
    }

    @Override
    public int compareTo(Object o) {
        Student student = (Student) o;
        if (!this.name.equals(student.name))
            return this.name.compareTo(student.name);
        if (this.age != student.age)
            return Integer.valueOf(this.age).compareTo(student.age);
        return Double.valueOf(this.scholarship).compareTo(student.scholarship) * -1; // * -1 for descending order

    }

    @Override
    public String toString() {
        return String.format("name = %6s, age = %2d, scholarship = %6.2f", name, age, scholarship);
    }
}
