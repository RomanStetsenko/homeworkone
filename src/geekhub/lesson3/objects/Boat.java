package geekhub.lesson3.objects;

import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson2.internal.Propeller;
import geekhub.lesson3.abstracts.Vehicle;
import geekhub.lesson3.internal.DieselEngine;
import geekhub.lesson3.internal.GasTank;

public class Boat extends Vehicle {

    public Boat(GasTank gasTank, DieselEngine dieselEngine) {
        super();
        forceAcceptor = new ForceAcceptor[]{new Propeller()};
        energyProvider = gasTank;
        forceProvider = dieselEngine;
        energyProvider.setForceProvider(forceProvider);
        forceProvider.setForceAcceptor(forceAcceptor);
        forceProvider.setEnergyProvider(energyProvider);
    }

    @Override
    public String toString() {
        return "Boat";
    }
}
