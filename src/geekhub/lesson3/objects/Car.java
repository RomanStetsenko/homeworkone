package geekhub.lesson3.objects;

import geekhub.lesson3.abstracts.Vehicle;
import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson3.internal.DieselEngine;
import geekhub.lesson3.internal.GasTank;
import geekhub.lesson2.internal.Wheel;

public class Car extends Vehicle {

    public Car(GasTank gasTank, DieselEngine dieselEngine) {
        super();
        forceAcceptor = new ForceAcceptor[]{new Wheel(), new Wheel(), new Wheel(), new Wheel()};
        energyProvider = gasTank;
        forceProvider = dieselEngine;
        energyProvider.setForceProvider(forceProvider);
        forceProvider.setForceAcceptor(forceAcceptor);
        forceProvider.setEnergyProvider(energyProvider);
    }


    @Override
    public String toString() {
        return "Car";
    }
}
