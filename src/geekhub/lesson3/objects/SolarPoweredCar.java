package geekhub.lesson3.objects;

import geekhub.lesson3.abstracts.Vehicle;
import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson2.internal.ElectricEngine;
import geekhub.lesson2.internal.SolarBattery;
import geekhub.lesson2.internal.Wheel;

public class SolarPoweredCar extends Vehicle {

    public SolarPoweredCar(SolarBattery solarBattery, ElectricEngine electricEngine) {
        super();
        forceAcceptor = new ForceAcceptor[]{new Wheel(), new Wheel(), new Wheel(), new Wheel()};
        energyProvider = solarBattery;
        forceProvider = electricEngine;
        energyProvider.setForceProvider(forceProvider);
        forceProvider.setForceAcceptor(forceAcceptor);
        forceProvider.setEnergyProvider(energyProvider);
    }

    @Override
    public String toString() {
        return "Solar-Powered Car";
    }
}
