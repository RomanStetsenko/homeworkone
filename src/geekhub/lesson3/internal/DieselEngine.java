package geekhub.lesson3.internal;

import geekhub.lesson3.abstracts.AbstractForceProvider;

public class DieselEngine extends AbstractForceProvider {

    public DieselEngine(int maxSpeed) {
        super(maxSpeed);
    }

    @Override
    public String toString() {
        return "Diesel Engine v2";
    }
}
