package geekhub.lesson7.threads;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {
    private List<String> imgTypes = Arrays.asList("jpg", "bmp", "gif", "png", "JPG", "GIF");

    //number of threads to download images simultaneously
    public static final int NUMBER_OF_THREADS = 10;

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    /**
     * Call this method to start download images from specified URL.
     *
     * @param urlToPage url of page that we are need to download
     * @throws java.io.IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        Page page = new Page(new URL(urlToPage));
        for (URL link : page.getImageLinks()) {
            if (isImageURL(link)) {
                executorService.execute(new ImageTask(link, folder));
            }
        }
    }

    /**
     * http://trinixy.ru/pics5/20141023/guess_who_01.jpg
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        String urlString = url.toString();
        int lastDotIndex = urlString.lastIndexOf('.') + 1;
        return urlString.length() - lastDotIndex > 2 && imgTypes.contains(urlString.substring(lastDotIndex, lastDotIndex + 3));
    }


}
