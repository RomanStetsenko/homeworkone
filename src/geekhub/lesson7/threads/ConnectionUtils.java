package geekhub.lesson7.threads;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url link that you want to download
     * @return byte array downloaded from url
     * @throws java.io.IOException
     */
    public static byte[] getData(URL url) throws IOException {
        byte[] byteArray = null;
        try (InputStream is = url.openStream();
             ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            int len;
            byte[] data = new byte[16 * 1024];
            while ((len = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, len);
            }
            byteArray = buffer.toByteArray();
        } catch (MalformedURLException e) {
            System.out.println("ConnectionUtils, Malformed URL: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("ConnectionUtils, I/O Error: " + e.getMessage());
        }
        return byteArray;
    }
}
