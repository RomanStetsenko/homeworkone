package geekhub.lesson5.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders;

    public SourceLoader() {
        sourceProviders = new ArrayList<>(Arrays.asList(new URLSourceProvider(),new FileSourceProvider()));
    }

    public String loadSource(String pathToSource) throws IOException {
        String result = "";
        for (SourceProvider each : sourceProviders){
            if (each.isAllowed(pathToSource)){
                result = each.load(pathToSource);
                break;
            }
        }
        if (result.length() < 1)
            throw new IOException("Bad url or file path");
        return result;
    }
}
