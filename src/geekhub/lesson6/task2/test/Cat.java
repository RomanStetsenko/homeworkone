package geekhub.lesson6.task2.test;

import geekhub.lesson6.task1.json.Ignore;
import geekhub.lesson6.task2.Constants;

import java.util.Date;

/**
 * Simple Cat that will be used for testing JSON serialization.
 */
public class Cat {
    private int age;
    @MeasureUnit(Constants.MILLIMETERS)
    private int furLength;
    @Ignore
    private Cat myself;
    @Ignore
    private Date birthDate;
    private String color;
    private Integer lengthCount;

    public Cat(int age, int furLength, Date birthDate, String color, Integer lengthCount) {
        this.age = age;
        this.furLength = furLength;
        this.myself = this;
        this.birthDate = birthDate;
        this.color = color;
        this.lengthCount = lengthCount;
    }
}