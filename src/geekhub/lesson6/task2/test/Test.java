package geekhub.lesson6.task2.test;

import geekhub.lesson6.task2.BeanRepresenter;

import java.util.Date;

public class Test {
    public static void main(String[] args) throws IllegalAccessException {
        Car car = new Car("blue", 190, "sedan", "RX-8");
        Cat cat = new Cat(3, 35, new Date(), "black", 4);
        Human human = new Human(180, "male", 22, 75.5, new Human(170, "female", 42, 50, null));
        BeanRepresenter.representObject(car);
        BeanRepresenter.representObject(cat);
        BeanRepresenter.representObject(human);
    }
}
