package geekhub.lesson6.task2.test;

import geekhub.lesson6.task1.json.Ignore;
import geekhub.lesson6.task2.Constants;

public class Car {
    private String color;
    @MeasureUnit(Constants.MILES_PER_HOUR)
    private int maxSpeed;
    private String type;
    private String model;
    @Ignore
    private Car thisCar;

    public Car(){}

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
        this.thisCar = this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (maxSpeed != car.maxSpeed) return false;
        if (!color.equals(car.color)) return false;
        if (!model.equals(car.model)) return false;
        if (!thisCar.equals(car.thisCar)) return false;
        if (!type.equals(car.type)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = color.hashCode();
        result = 31 * result + maxSpeed;
        result = 31 * result + type.hashCode();
        result = 31 * result + model.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
