package geekhub.lesson6.task2.test;

import geekhub.lesson6.task1.json.Ignore;
import geekhub.lesson6.task2.Constants;

public class Human {
    @MeasureUnit(Constants.CENTIMETERS)
    private int height;
    private String male;
    private Integer age;
    @MeasureUnit(Constants.KILOGRAMS)
    private double weight;
    @Ignore
    private Human parent;

    public Human() {}

    public Human(int height, String male, Integer age, double weight, Human parent) {
        this.height = height;
        this.male = male;
        this.age = age;
        this.weight = weight;
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        if (height != human.height) return false;
        if (Double.compare(human.weight, weight) != 0) return false;
        if (!age.equals(human.age)) return false;
        if (!male.equals(human.male)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = height;
        result = 31 * result + male.hashCode();
        result = 31 * result + age.hashCode();
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                "height=" + height +
                ", male='" + male + '\'' +
                ", age=" + age +
                ", weight=" + weight +
                '}';
    }
}
