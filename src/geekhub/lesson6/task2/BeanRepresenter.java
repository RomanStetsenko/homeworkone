package geekhub.lesson6.task2;

import geekhub.lesson6.task1.json.Ignore;
import geekhub.lesson6.task2.test.MeasureUnit;

import java.lang.reflect.Field;

public class BeanRepresenter {

    public static void representObject(Object o) throws IllegalAccessException {
        System.out.println("\n");
        Class clazz = o.getClass();
        System.out.printf("%s%n", clazz.getSimpleName());
        Field[] fields = clazz.getDeclaredFields();
        for (Field each : fields) {
            each.setAccessible(true);
            String measureUnit = "";
            if (!each.isAnnotationPresent(Ignore.class)){
                if (each.isAnnotationPresent(MeasureUnit.class))
                    measureUnit = ((MeasureUnit) each.getDeclaredAnnotations()[0]).value();
                System.out.printf("%-11s %s%s%n", each.getName(), each.get(o), measureUnit);
            }
        }
    }

}
