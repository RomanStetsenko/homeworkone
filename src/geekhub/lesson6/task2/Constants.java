package geekhub.lesson6.task2;

public class Constants {
    private Constants(){}

    public static final String MILLIMETERS = "mm";
    public static final String MILES_PER_HOUR = "mph";
    public static final String CENTIMETERS = "cm";
    public static final String KILOGRAMS = "kg";
}
