package geekhub.lesson8.db.storage;


import geekhub.lesson8.db.objects.Entity;
import geekhub.lesson8.db.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link geekhub.lesson8.db.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link geekhub.lesson8.db.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        //implement me according to interface by using extractResult method
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try(Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try(Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql) == 1;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        String sql;
        String delimiter = "";
        String valuesDelimiter = "\"";

        if (entity.isNew()) {
            StringBuilder columnNames = new StringBuilder();
            StringBuilder columnValues = new StringBuilder();

            for (Map.Entry<String, Object> entry : data.entrySet()) {
                columnNames.append(delimiter).append(entry.getKey());
                columnValues.append(valuesDelimiter);
                if (entry.getValue() instanceof Boolean){
                    columnValues.append((Boolean) entry.getValue() ? 1: 0);
                } else {
                    columnValues.append(entry.getValue());
                }
                delimiter = ", ";
                valuesDelimiter = "\", \"";
            }
            columnValues.append("\"");
            sql = "INSERT INTO " + entity.getClass().getSimpleName() + " (" + columnNames.toString() +  ") VALUES (" + columnValues.toString() + ")";
            try(PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                int affectedRows = statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                if (affectedRows == 0) {
                    throw new SQLException("Creating failed, no rows affected.");
                }
                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        entity.setId(generatedKeys.getInt(1));
                    } else {
                        throw new SQLException("Creating user failed, no ID obtained.");
                    }
                }
            }
            //need to define right SQL query to create object
        } else {
            StringBuilder setNewValuesString = new StringBuilder();

            for (Map.Entry<String, Object> entry : data.entrySet()) {
                setNewValuesString.append(delimiter).append(entry.getKey()).append(" = '");
                if (entry.getValue() instanceof Boolean){
                    setNewValuesString.append((Boolean) entry.getValue() ? 1: 0);
                } else {
                    setNewValuesString.append(entry.getValue());
                }
                setNewValuesString.append("'");
                delimiter = ", ";
            }

            sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + setNewValuesString.toString() + " WHERE id = " + entity.getId();
            try(Statement statement = connection.createStatement()) {
              statement.executeUpdate(sql);
            }
            //need to define right SQL query to update object
        }
        //implement me, need to save/update object and update it with new id if it's a creation
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> map = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields){
            boolean isAccessible = field.isAccessible();
            if (!isAccessible){
                field.setAccessible(true);
            }
            if (!field.isAnnotationPresent(Ignore.class)) {
                map.put(field.getName(),field.get(entity));
            }
            if (!isAccessible){
                field.setAccessible(false);
            }
        }
        return map;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> list = new ArrayList<>();
        List<Field> fieldList = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        fieldList.addAll(Arrays.asList(clazz.getSuperclass().getDeclaredFields()));
        while (resultset.next()){
            T newInstance = clazz.newInstance();
            for (Field field : fieldList){
                if (!field.isAccessible()){
                    field.setAccessible(true);
                }
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.set(newInstance, resultset.getObject(field.getName()));
                }
            }
            list.add(newInstance);
        }
        return list;
    }
}
