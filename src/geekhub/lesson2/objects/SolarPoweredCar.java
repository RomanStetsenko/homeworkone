package geekhub.lesson2.objects;

import geekhub.lesson2.abstracts.Vehicle;
import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson2.internal.ElectricEngine;
import geekhub.lesson2.internal.SolarBattery;
import geekhub.lesson2.internal.Wheel;

public class SolarPoweredCar extends Vehicle {

    public SolarPoweredCar() {
        super();
        forceAcceptor = new ForceAcceptor[]{new Wheel(), new Wheel(), new Wheel(), new Wheel()};
        energyProvider = new SolarBattery();
        forceProvider = new ElectricEngine();
        energyProvider.setForceProvider(forceProvider);
        forceProvider.setForceAcceptor(forceAcceptor);
        forceProvider.setEnergyProvider(energyProvider);
    }

    @Override
    public String toString() {
        return "Solar-Powered Car";
    }
}
