package geekhub.lesson2.objects;

import geekhub.lesson2.abstracts.Vehicle;
import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson2.internal.DieselEngine;
import geekhub.lesson2.internal.GasTank;
import geekhub.lesson2.internal.Propeller;

public class Boat extends Vehicle {

    public Boat() {
        super();
        forceAcceptor = new ForceAcceptor[]{new Propeller()};
        energyProvider = new GasTank();
        forceProvider = new DieselEngine();
        energyProvider.setForceProvider(forceProvider);
        forceProvider.setForceAcceptor(forceAcceptor);
        forceProvider.setEnergyProvider(energyProvider);
    }

    @Override
    public String toString() {
        return "Boat";
    }
}
