package geekhub.lesson2.internal;

import geekhub.lesson2.abstracts.AbstractForceProvider;

public class DieselEngine extends AbstractForceProvider {
    @Override
    public String toString() {
        return "Diesel Engine";
    }
}
