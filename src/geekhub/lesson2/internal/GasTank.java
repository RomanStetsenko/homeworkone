package geekhub.lesson2.internal;

import geekhub.lesson2.abstracts.AbstractEnergyProvider;
import geekhub.lesson3.exceptions.OverflowEnergyLevelException;

import java.util.Timer;
import java.util.TimerTask;

public class GasTank extends AbstractEnergyProvider {

    public GasTank() {
    }

    public GasTank(int volume) {
        super(volume);
    }

    public void startTimer() {
        timer = new Timer();
        timer.schedule(new GasTankTimer(toString()), 0, 1000);
    }

    public void pauseTimer() {
        timer.cancel();
//        timer.purge();
    }

    @Override
    public String toString() {
        return String.format("Gas-tank(%d)", volume);
    }

    @Override
    public void addEnergy(int extraEnergyValue) throws OverflowEnergyLevelException {
        System.out.println("TODO");
    }

    private class GasTankTimer extends TimerTask {
        String energyProviderName;

        public GasTankTimer(String energyProviderName) {
            this.energyProviderName = energyProviderName;
        }

        @Override
        public void run() {
            if (volume < 1) {
                System.out.println(energyProviderName + " is empty");
                pauseTimer();
                forceProvider.stop();
            }
            volume -= fuelConsumption;
            if (volume % 5 == 0) {
                if (volume < 0)
                    volume = 0;
                System.out.println("Tank level is:" + volume);
            }
        }
    }
}
