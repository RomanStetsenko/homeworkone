package geekhub.lesson2.internal;

import geekhub.lesson2.abstracts.AbstractEnergyProvider;
import geekhub.lesson3.exceptions.OverflowEnergyLevelException;

import java.util.Timer;
import java.util.TimerTask;

public class SolarBattery extends AbstractEnergyProvider {

    public SolarBattery() {}

    public SolarBattery(int volume) {
        super(volume);
    }

    public void startTimer() {
        timer = new Timer();
        timer.schedule(new SolarBatteryTimer(toString()), 0, 1000);
    }

    public void pauseTimer() {
        timer.cancel();
//        timer.purge();
    }

    @Override
    public String toString() {
        return "Solar Battery";
    }

    @Override
    public void addEnergy(int extraEnergyValue) throws OverflowEnergyLevelException {
        System.out.println("TODO");
    }

    private class SolarBatteryTimer extends TimerTask {
        String energyProviderName;
        int counter = 0;
        
        public SolarBatteryTimer(String energyProviderName) {
            this.energyProviderName = energyProviderName;
        }

        @Override
        public void run() {

            if (volume < 1) {
                System.out.println(energyProviderName +" is empty");
                pauseTimer();
                forceProvider.stop();
            }
            volume-=fuelConsumption;
                ++counter;
                if (counter % 10 == 0){
                    volume += 5;
                    System.out.println("+5 from solar-power");
                    counter = 0;
                }
            if (volume % 5 == 0)
                System.out.println("Tank level is:" + volume);
        }
    }
}
