package geekhub.lesson2.internal;

import geekhub.lesson2.abstracts.AbstractForceProvider;

public class ElectricEngine extends AbstractForceProvider {
    @Override
    public String toString() {
        return "Electric Engine";
    }
}
