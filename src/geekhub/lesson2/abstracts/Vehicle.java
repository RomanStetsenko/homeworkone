package geekhub.lesson2.abstracts;

import geekhub.lesson2.interfaces.Driveable;
import geekhub.lesson2.interfaces.EnergyProvider;
import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson2.interfaces.ForceProvider;
import geekhub.lesson3.exceptions.EmptyEnergyProviderException;
import geekhub.lesson3.exceptions.OverflowEnergyLevelException;

import java.util.Random;

public abstract class Vehicle implements Driveable {
    protected ForceProvider forceProvider;
    protected ForceAcceptor forceAcceptor[];
    protected EnergyProvider energyProvider;
    protected String directions[] = {"South", "West", "East", "North"};
    protected Random random = new Random();
    protected String currentDirection;

    protected Vehicle() {
        currentDirection = directions[random.nextInt(directions.length)];
    }

    @Override
    public void showEnergyLevel() {
        System.out.println("Energy Level is:" + energyProvider.getVolume());
    }

    @Override
    public void starting() {
        if (!forceProvider.isWorking()){
            if (energyProvider.getVolume() > 0) {
                forceProvider.starting();
            } else {
                System.out.println(energyProvider.toString()+" is empty");
            }
        } else
            System.out.println(forceProvider.toString() + " is already works");
    }

    @Override
    public void accelerate() {
        if (forceProvider.isWorking()){
            forceProvider.accelerating();
        } else
            System.out.println(forceProvider.toString() + " is not works");
    }

    @Override
    public void brake() {
        if (forceProvider.isWorking()){
            forceProvider.braking();
        } else
            System.out.println(toString() + " is already stopped");
    }

    @Override
    public void turn() {
        if (forceProvider.isWorking()) {
            String newDirection;
            do {
                newDirection = directions[random.nextInt(directions.length)];
            } while (newDirection.equals(currentDirection));
            currentDirection = newDirection;
            System.out.println(toString() + " is turning to the " + newDirection);
        } else
            System.out.println("Can't turn while "+ forceProvider.toString() +" is not working");
    }

    @Override
    public void stop() {
        if (forceProvider.isWorking()){
            forceProvider.stop();
        } else
            System.out.println("Engine is already stopped");
    }

    @Override
    public void powerfulAccelerate() {
        accelerate();
        accelerate();
    }

    @Override
    public void hardBraking() {
        brake();
        brake();
    }

    @Override
    public void addTenEnergyUnits() throws OverflowEnergyLevelException {
        energyProvider.addEnergy(10000);
    }

    @Override
    public void drive10miles() throws EmptyEnergyProviderException {
        System.out.println("to lesson 3");
    }

    @Override
    public void showTravelledDistance() {
        System.out.format(" Travelled distance %f miles%n", forceProvider.getTravelledDistance());
    }
}
