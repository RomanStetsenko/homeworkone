package geekhub.lesson2.abstracts;

import geekhub.lesson2.interfaces.EnergyProvider;
import geekhub.lesson2.interfaces.ForceAcceptor;
import geekhub.lesson2.interfaces.ForceProvider;

import java.util.Timer;
import java.util.TimerTask;

public abstract class AbstractForceProvider implements ForceProvider {
    private boolean isWorking;
    protected ForceAcceptor forceAcceptor[];
    protected EnergyProvider energyProvider;
    protected int currentSpeed = 0;
    protected double travelledDistance = 0;
    private Timer timer;
    protected int maxSpeed;
    protected double wayLimit;

    public AbstractForceProvider(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    protected AbstractForceProvider() {
    }

    public void setForceAcceptor(ForceAcceptor forceAcceptor[]) {
        this.forceAcceptor = forceAcceptor;
    }

    public void setEnergyProvider(EnergyProvider energyProvider) {
        this.energyProvider = energyProvider;
    }

    public boolean isWorking() {
        return isWorking;
    }

    @Override
    public int getCurrentSpeed() {
        return currentSpeed * 5;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public double getTravelledDistance() {
        return travelledDistance;
    }

    @Override
    public void setWayLimit(double limit) {
        wayLimit = limit;
    }


    @Override
    public void starting() {
        isWorking = true;
        energyProvider.startEnergyFeeding();
        System.out.println(toString() + " is working");
        timer = new Timer();
        timer.schedule(new DistanceTimer(), 0, 1000);
    }

    @Override
    public void accelerating() {
        currentSpeed++;
        energyProvider.increaseEnergyFeeding();
        for (ForceAcceptor each : forceAcceptor) {
            if (!each.getState())
                each.roll();
        }
        System.out.format("Your speed is %d miles per hour%n", currentSpeed * 5);
    }

    @Override
    public void braking() {
        currentSpeed--;
        energyProvider.decreaseEnergyFeeding();
        if (currentSpeed < 1) {
            for (ForceAcceptor each : forceAcceptor) {
                if (each.getState())
                    each.stop();
            }
        }
        System.out.format("Your speed is %d miles per hour%n", currentSpeed * 5);
    }

    public void stop() {
        System.out.println("Engine stops");
        timer.cancel();
        isWorking = false;
        for (ForceAcceptor each : forceAcceptor) {
            if (each.getState())
                each.stop();
        }
        energyProvider.stopEnergyFeeding();
        currentSpeed = 0;
        System.out.format("Travelled distance is %f miles %n", travelledDistance);
    }

    private class DistanceTimer extends TimerTask {
        double tempDistance;

        @Override
        public void run() {
            if (currentSpeed > 0)
                travelledDistance += (double) (currentSpeed * 5) / 3600;
            if (wayLimit > 0) {
                tempDistance += (double) (currentSpeed * 5) / 3600;
                if (tempDistance >= wayLimit){
                    stop();
                    tempDistance = wayLimit = 0;
                }

            }
//            System.out.format("covered distance: %f current speed: %d current covered distance %f %n ",travelledDistance, currentSpeed*5, (double)(currentSpeed * 5)/3600);
        }
    }
}
