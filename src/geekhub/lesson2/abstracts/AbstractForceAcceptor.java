package geekhub.lesson2.abstracts;

import geekhub.lesson2.interfaces.ForceAcceptor;

public abstract class AbstractForceAcceptor implements ForceAcceptor {

    private boolean isRolling = false;

    public boolean getState() {
        return isRolling;
    }

    public void roll() {
        if (!isRolling){
            isRolling = true;
            System.out.printf("%s %s is rolling%n",getClass().getSimpleName(), Integer.toHexString(hashCode()).substring(0,4));
        }
    }

    public void stop() {
        if (isRolling){
            isRolling = false;
            System.out.printf("%s %s stops%n",getClass().getSimpleName(), Integer.toHexString(hashCode()).substring(0,4));
        }
    }
}
