package geekhub.lesson2.abstracts;

import geekhub.lesson2.interfaces.EnergyProvider;
import geekhub.lesson2.interfaces.ForceProvider;

import java.util.Timer;

public abstract class AbstractEnergyProvider implements EnergyProvider, Comparable {
    protected int volume = 75000;
    protected int maxVolume = 85000;
    protected int fuelConsumption = 1;
    protected Timer timer;
    protected ForceProvider forceProvider;

    protected AbstractEnergyProvider(int volume) {
        this.volume = volume;
        if (volume > maxVolume)
            maxVolume = volume;
    }

    protected AbstractEnergyProvider() {}

    public void setForceProvider(ForceProvider forceProvider) {
        this.forceProvider = forceProvider;
    }

    public void startEnergyFeeding() {
        startTimer();
        System.out.printf("Volume of the %s is: %d%n", getClass().getSimpleName(), volume);
    }

    public void stopEnergyFeeding() {
        pauseTimer();
        fuelConsumption = 1;
    }

    public void increaseEnergyFeeding(){
        fuelConsumption++;
        System.out.printf("Energy feeding is %d per second%n", fuelConsumption);
    }

    public void decreaseEnergyFeeding(){
        if (fuelConsumption > 1)
            fuelConsumption--;
        System.out.printf("Energy feeding is %d per second%n", fuelConsumption);
    }

    @Override
    public int getVolume() {
        return volume;
    }

    protected abstract void startTimer();

    protected abstract void pauseTimer();

    @Override
    public int compareTo(Object o) {
        AbstractEnergyProvider energyProvider = (AbstractEnergyProvider) o;
        if (this.volume < energyProvider.volume) {
            return -1;
        } else if (this.volume > energyProvider.volume) {
            return 1;
        } else return 0;
    }
}
