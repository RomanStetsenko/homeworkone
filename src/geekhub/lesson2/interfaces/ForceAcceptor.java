package geekhub.lesson2.interfaces;

public interface ForceAcceptor {
    void roll();
    void stop();
    boolean getState();
}
