package geekhub.lesson2.interfaces;

public interface ForceProvider {
    void starting();
    void accelerating();
    void braking();
    void stop();
    void setForceAcceptor(ForceAcceptor forceAcceptor[]);
    void setEnergyProvider(EnergyProvider energyProvider);
    boolean isWorking();
    int getCurrentSpeed();
    int getMaxSpeed();
    double getTravelledDistance();
    void setWayLimit(double limit);
}
