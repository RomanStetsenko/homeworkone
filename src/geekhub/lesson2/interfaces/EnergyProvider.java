package geekhub.lesson2.interfaces;

import geekhub.lesson3.exceptions.OverflowEnergyLevelException;

public interface EnergyProvider {
    void startEnergyFeeding();
    void stopEnergyFeeding();
    void increaseEnergyFeeding();
    void decreaseEnergyFeeding();
    int getVolume();
    void setForceProvider(ForceProvider forceProvider);
    void addEnergy(int extraEnergyValue) throws OverflowEnergyLevelException;
}
