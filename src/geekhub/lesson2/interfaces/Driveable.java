package geekhub.lesson2.interfaces;

import geekhub.lesson3.exceptions.EmptyEnergyProviderException;
import geekhub.lesson3.exceptions.OverflowEnergyLevelException;

public interface Driveable {
    void starting();
    void accelerate();
    void brake();
    void turn();
    void stop();
    void addTenEnergyUnits() throws OverflowEnergyLevelException;
    void drive10miles() throws EmptyEnergyProviderException;
    void showTravelledDistance();
    void showEnergyLevel();
    void powerfulAccelerate();
    void hardBraking();
}
