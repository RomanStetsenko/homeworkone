package geekhub.lesson2;

import geekhub.lesson2.interfaces.Driveable;
import geekhub.lesson2.objects.Boat;
import geekhub.lesson2.objects.Car;
import geekhub.lesson2.objects.SolarPoweredCar;

import java.util.Scanner;

public class Main {
    private static Driveable driveable;

    private static void printMainMenu() {
        System.out.println("Input \"1\" to choose car");
        System.out.println("Input \"2\" to choose solar-powered car");
        System.out.println("Input \"3\" to choose boat");
        System.out.println("Input \"q\" for finish\n");
    }

    private static void printActionsMenu() {
        System.out.println("Input \"s\" for engine starting");
        System.out.println("Input \"go\" to start or increase moving");
        System.out.println("Input \"turn\" to turn the vehicle");
        System.out.println("Input \"brake\" for braking");
        System.out.println("Input \"stop\" to stop");
        System.out.println("Input \"up\" to select another vehicle");
        System.out.println("Input \"q\" for finish\n");
    }

    public static void main(String[] args) {
        Car car = new Car();
        Boat boat = new Boat();
        SolarPoweredCar solarPoweredCar = new SolarPoweredCar();
        Scanner sc = new Scanner(System.in);

        while (true) {
            printMainMenu();
            String line = sc.nextLine();
            boolean isSelected;
            if (line.equalsIgnoreCase("1")) {
                driveable = car;
                isSelected = true;
            } else if (line.equalsIgnoreCase("2")) {
                driveable = solarPoweredCar;
                isSelected = true;
            } else if (line.equalsIgnoreCase("3")) {
                driveable = boat;
                isSelected = true;
            } else if (line.equalsIgnoreCase("q")) {
                break;
            } else {
                isSelected = false;
                System.out.println("Try again");
            }

            if (isSelected) {
                printActionsMenu();
                while (true) {
                    String action = sc.nextLine();
                    if (action.equalsIgnoreCase("up")) break;
                    else if (action.equalsIgnoreCase("q")) System.exit(0);
                    else if (action.equalsIgnoreCase("s")) driveable.starting();
                    else if (action.equalsIgnoreCase("go")) driveable.accelerate();
                    else if (action.equalsIgnoreCase("brake")) driveable.brake();
                    else if (action.equalsIgnoreCase("turn")) driveable.turn();
                    else if (action.equalsIgnoreCase("stop")) driveable.stop();
                    else System.out.println("Try again");
                }
            }
        }
    }
}
